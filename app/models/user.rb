class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  # :registerable allows others to signup
  devise :registerable, :database_authenticatable, :recoverable, :rememberable, :validatable
end
