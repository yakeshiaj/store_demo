##README

#Planning our application
1. Answer Questions
	- What are we building?
	- Who are we building it for?
	- what features do we need to have?
2. User Stories
3. Model our Data
4. Layouts

#Questions
What are we building? We are building a small start to an e-commerce application with admin user authentication. The application of which I call “Juiced & Drippin” will revolve around selling and purchasing variety of swag. The app will use a series of models to formulate both the cart and items of which are for sale. Along the way, we will craft some handy helpers that perform calculations to total the items within our cart as well as remember who a user is if that user adds an item to a cart without being logged in.

A admin user who has an account can post swag for sale.

Who are we building it for? We are building it for ourselves and the community. Sharing what we are learning by blogging is a great way to learn for ourselves while teaching others in the process. Show potential employers that we have knowledge and experience.

what features do we need to have?
	- Products
		- Create / Edit / Destroy
		- Markdown
  - Checkout
   - Checkout form
	- Contact
		- Contact form
		- SendGrid: Email Delivery Service
	- User (Devise)

#User Stories
As a , I want to be able to , so that .

- As a user, I want to be able to create products, so that I can share what I am learning on my blog.
- As a user, I want to be able to perform CRUD operations, so that I can manage my inventory.
- As a user, I want to be able to write product's description in markdown format, so that writing descriptions can be efficient and user-friendly.
- As a user, I want to show the visitors and potential employers examples of my work and/or apps I have built.
- As a user, I want to be able to have visitors contact me through a form on my site.

#Modeling our Data
- Product
	- title:string
	- description:string
	- price:decimal{'15,2'}

- ProductVariant
	- title:string
	- price:decimal{'15,2'}

- Category
	- title:string

- Order
	- firstname:string
	- lastname:string
	- subtotal:decimal{'15,2'}

- Order Item
	- quantity:string
	- price:decimal{'15,2'}

#Layouts
- Store#index
- Product#index
- Product#show
- Checkout#index
- Contact
